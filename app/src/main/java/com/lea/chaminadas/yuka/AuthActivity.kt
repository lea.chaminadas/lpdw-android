package com.lea.chaminadas.yuka

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.lea.chaminadas.yuka.home.MainActivity
import kotlinx.android.synthetic.main.activity_auth.*

class AuthActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
    }

    public override fun onStart() {
        super.onStart()

        auth = FirebaseAuth.getInstance()

        registration_button.setOnClickListener(View.OnClickListener {
            val email = user_email.text.toString()
            val password = user_psw.text.toString()

            auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("CREATE_USER", "createUserWithEmail:success")

                        val intent = Intent(this, MainActivity::class.java)
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent)
                        finish()
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w("CREATE_USER", "createUserWithEmail:failure", task.exception)
                        Toast.makeText(baseContext, "Authentication failed.",
                            Toast.LENGTH_SHORT).show()
                    }
                }
        })

        login_button.setOnClickListener(View.OnClickListener {

            val email = user_email.text.toString()
            val password = user_psw.text.toString()

            auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("LOGIN_USER", "signInWithEmail:success")

                        val intent = Intent(this, MainActivity::class.java)
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        startActivity(intent)
                        finish()
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w("LOGIN_USER", "signInWithEmail:failure", task.exception)
                        Toast.makeText(baseContext, "Authentication failed.",
                            Toast.LENGTH_SHORT).show()
                    }

                    // ...
                }
        })

        dont_have_account_button.setOnClickListener(View.OnClickListener {
            switchUI("REGISTRATION")
        })

        already_have_account_button.setOnClickListener(View.OnClickListener {
            switchUI("LOGIN")
        })
    }

    fun switchUI(type: String){
        when(type){
            "REGISTRATION" -> {
                login_button.visibility = View.GONE
                registration_button.visibility = View.VISIBLE
                dont_have_account_button.visibility = View.GONE
                already_have_account_button.visibility = View.VISIBLE
                already_have_account_button_label.visibility = View.VISIBLE
            }
            "LOGIN" -> {
                login_button.visibility = View.VISIBLE
                registration_button.visibility = View.GONE
                dont_have_account_button.visibility = View.VISIBLE
                already_have_account_button.visibility = View.GONE
                already_have_account_button_label.visibility = View.GONE
            }
        }

    }

}
