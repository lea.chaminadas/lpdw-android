package com.lea.chaminadas.yuka.product

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.lea.chaminadas.yuka.R

class ProductFragmentAdapter(fm: FragmentManager, private val product: Product, context: Context) : FragmentPagerAdapter(fm) {
    val context: Context = context
    override fun getItem(position: Int): Fragment {
        return when(position){
            0 -> ProductFragment.newInstance(product)
            1 -> ProductNutriLevelFragment.newInstance(product)
            2 -> ProductNutriInfoFragment.newInstance(product)
            else -> throw Exception("WTF")
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> context.resources.getString(R.string.fiche)
            1 -> context.resources.getString(R.string.nutrition)
            2 -> context.resources.getString(R.string.nutrition_info)
            else -> throw Exception("WTF")
        }
    }

}