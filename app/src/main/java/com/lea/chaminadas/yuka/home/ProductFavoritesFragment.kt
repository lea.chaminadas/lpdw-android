package com.lea.chaminadas.yuka.home


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.lea.chaminadas.yuka.R
import com.lea.chaminadas.yuka.product.ProductWithFav
import kotlinx.android.synthetic.main.fragment_product_list.*


class ProductFavoritesFragment : BaseFragment(){


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_product_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        product_list.layoutManager = LinearLayoutManager(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val user = auth.currentUser
        if(user == null){

        }else {
            reference = FirebaseDatabase.getInstance().getReference("/users/${user.uid}/favorites")

            listener = reference!!.addValueEventListener(object :
                ValueEventListener {
                override fun onDataChange(p0: DataSnapshot) {
                    val products = mutableListOf<ProductWithFav>()

                    for (history in p0.children) {

                        var history = history.getValue(History::class.java)
                        if(history != null){
                            products.add(ProductWithFav(true, history.toProduct()))
                        }
                    }

                    displayList(products)
                }

                override fun onCancelled(p0: DatabaseError) {
                    throw p0.toException()
                }
            })
        }
    }

    companion object {
        fun newInstance() : ProductFavoritesFragment {
            val fragment = ProductFavoritesFragment()
            return fragment
        }
    }
}
