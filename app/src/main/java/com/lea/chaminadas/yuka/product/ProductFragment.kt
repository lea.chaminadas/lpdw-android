package com.lea.chaminadas.yuka.product

import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.lea.chaminadas.yuka.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_product.*

class ProductFragment : Fragment() {
    lateinit var product: Product

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (arguments == null || arguments?.containsKey("product") == false) {
            throw Exception("Product is missing")
        }
        product = arguments!!.getParcelable("product")

        return inflater.inflate(R.layout.fragment_product, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        product_title.text = product.name
        product_brand.text = product.brand.joinToString()
        product_bar_code.text = Html.fromHtml(getString(R.string.bar_code, product.barCode))
        product_quantity.text = Html.fromHtml(getString(R.string.quantity, product.quantity))
        product_sold.text = Html.fromHtml(getString(R.string.sold, product.countrySold.joinToString()))
        val ingredientsSpanned =
            """_([^*])(.*?)([^*])_""".toRegex().replace(product.ingredients.joinToString(), "<b>$1$2$3</b>")

        product_ingredients.text = Html.fromHtml(
            getString(
                R.string.ingredients,
                ingredientsSpanned
            )
        )
        product_allergies.text = Html.fromHtml(getString(R.string.allergies, product.allergies.joinToString()))
        product_additives.text = Html.fromHtml(
            getString(
                R.string.additives,
                product.additives?.entries?.map { (k, v) -> "$k: $v" }?.joinToString()
            )
        )
        product_nutri_score.setImageResource(
            resources.getIdentifier(
                "nutri_score_${product.nutriScore?.toLowerCase()}",
                "drawable",
                context?.packageName
            )
        )
        Picasso.get().load(product.url).into(product_image)
    }


    companion object {
        fun newInstance(product: Product): ProductFragment {
            val fragment = ProductFragment()
            val args = Bundle()
            args.putParcelable("product", product)
            fragment.arguments = args
            return fragment
        }
    }
}

