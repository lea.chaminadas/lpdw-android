package com.lea.chaminadas.yuka.home

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.firebase.auth.FirebaseAuth
import com.lea.chaminadas.yuka.*
import com.lea.chaminadas.yuka.product.ProductActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(){

    private lateinit var auth: FirebaseAuth

    @Suppress("DEPRECATION")
    override fun onCreate(savedInstanceState: Bundle?) {
        auth = FirebaseAuth.getInstance()

        val currentUser = auth.currentUser
        if(currentUser == null){
            val k = Intent(applicationContext, AuthActivity::class.java)
            startActivity(k)
        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.setBackgroundDrawable(ContextCompat.getDrawable(this,
            R.drawable.bg_toolbar
        ))
        supportActionBar?.setTitle(R.string.main_activity_title)

        main_navigation.setOnNavigationItemSelectedListener {

            val fragment = when(it.itemId) {
                R.id.main_nav_bar_cart -> ProductListFragment.newInstance()
                R.id.main_nav_bar_bookmarks -> ProductFavoritesFragment.newInstance()
                R.id.main_nav_bar_stats -> StatsFragment.newInstance(
                    "stats"
                )
                R.id.main_nav_bar_profile -> ProfileFragment.newInstance()
                else -> throw Exception("c'est la cata")
            }

            supportFragmentManager
                .beginTransaction()
                .replace(R.id.main_container, fragment)
                .commitAllowingStateLoss()

            true
        }

        main_navigation.selectedItemId = R.id.main_nav_bar_cart

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.menu_main_activity, menu)

        return super.onCreateOptionsMenu(menu)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            R.id.menu_item_barcode -> {
                startActivityForResult(Intent("com.google.zxing.client.android.SCAN").apply {
                    putExtra("SCAN_FORMATS", "EAN_13")
                }, 100)
            }
        }

        return super.onOptionsItemSelected(item)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        var barcode = data?.getStringExtra("SCAN_RESULT")
        if(!barcode.isNullOrEmpty()){
            val k = Intent(applicationContext, ProductActivity::class.java)
            k.putExtra("barcode", barcode)
            startActivity(k)
        }
    }

}

