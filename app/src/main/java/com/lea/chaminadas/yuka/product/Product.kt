package com.lea.chaminadas.yuka.product

import android.os.Parcelable
import com.lea.chaminadas.yuka.home.History
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Product(
    val name: String,
    val brand: List<String?>,
    val barCode: String,
    val nutriScore: String?,
    val url: String?,
    val quantity: String,
    val countrySold: List<String?>,
    val ingredients: List<String?>,
    val allergies: List<String?>,
    val additives: Map<String, String>?,
    val nutritionFacts: NutritionFacts?
) : Parcelable {
    fun toHistory(id: Long) = History(
        id = id,
        productName = name,
        productBrands = brand ?: listOf(""),
        productBarcode = barCode,
        productImageUrl = url,
        productNutriScore = nutriScore ?: "N/A",
        productEnergy = nutritionFacts?.energy?.quantity
    )
}

@Parcelize
data class NutritionFacts(
    val energy: NutritionFactsItem?,
    val fat: NutritionFactsItem?,
    val saturatedFat: NutritionFactsItem?,
    val carbohydrate: NutritionFactsItem?,
    val sugar: NutritionFactsItem?,
    val fibers: NutritionFactsItem?,
    val proteins: NutritionFactsItem?,
    val salt: NutritionFactsItem?,
    val sodium: NutritionFactsItem?
) : Parcelable

@Parcelize
data class NutritionFactsItem(
    val unit: String?,
    val quantity: String?,
    val quantity100: String?
) : Parcelable