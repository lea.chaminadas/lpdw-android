package com.lea.chaminadas.yuka.home


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.lea.chaminadas.yuka.R
import kotlinx.android.synthetic.main.fragment_profile.*


class ProfileFragment : Fragment() {
    val auth = FirebaseAuth.getInstance()
    lateinit var authListener: FirebaseAuth.AuthStateListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        authListener = object : FirebaseAuth.AuthStateListener {
            override fun onAuthStateChanged(firebaseAuth: FirebaseAuth) {
                if (this@ProfileFragment.auth.currentUser == null) {
                    Log.e("notify", "notified")
                    val k = Intent(activity, MainActivity::class.java)
                    k.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(k)
                    activity?.finish()
                }
            }
        }


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onStart() {
        super.onStart()
        auth.addAuthStateListener(authListener) //firebaseAuth is of class FirebaseAuth
    }

    override fun onStop() {
        super.onStop()
        if (authListener != null) {
            auth.removeAuthStateListener(authListener)
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)



        logout_button.setOnClickListener(View.OnClickListener {
            auth.signOut()

        })
    }

    companion object {
        fun newInstance(): ProfileFragment {
            val fragment = ProfileFragment()
            return fragment
        }
    }

}
