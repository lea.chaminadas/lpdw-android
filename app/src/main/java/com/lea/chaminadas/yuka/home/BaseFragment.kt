package com.lea.chaminadas.yuka.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.lea.chaminadas.yuka.R
import com.lea.chaminadas.yuka.product.Product
import com.lea.chaminadas.yuka.product.ProductActivity
import com.lea.chaminadas.yuka.product.ProductWithFav
import kotlinx.android.synthetic.main.fragment_product_list.*


open class BaseFragment : Fragment(), OnProductClickListener {

    var listener: ValueEventListener? = null
    var reference: DatabaseReference? = null
    val db = FirebaseDatabase.getInstance()
    val auth = FirebaseAuth.getInstance()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_product_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        product_list.layoutManager = LinearLayoutManager(context)
        empty_list_view_button.setOnClickListener(View.OnClickListener {
            startActivityForResult(Intent("com.google.zxing.client.android.SCAN").apply {
                putExtra("SCAN_FORMATS", "EAN_13")
            }, 100)
        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    fun displayList(products: List<ProductWithFav>){
        if(products.isEmpty()){
            empty_list_view.visibility = View.VISIBLE
            product_list.visibility = View.GONE
        } else {
            product_list.adapter = ProductListAdapter(products, this)
            empty_list_view.visibility = View.GONE
            product_list.visibility = View.VISIBLE
        }
    }

    override fun onPause() {
        super.onPause()

        if (listener != null) {
            reference?.removeEventListener(listener!!)
        }
    }


    override fun onProductClicked(product: Product) {
        val k = Intent(context, ProductActivity::class.java)
        k.putExtra("barcode", product.barCode)
        startActivity(k)
    }

    override fun onBookmarkClicked(product: Product, view: View) {
        val user = auth.currentUser
        if(user == null){

        }else{

            db.getReference("users/${user.uid}/favorites").child(product.barCode).addListenerForSingleValueEvent(object :
                ValueEventListener {
                override fun onDataChange(p0: DataSnapshot) {
                    var bookmarkText = ""
                    if (p0.exists()) {
                        db.getReference("users/${user.uid}/favorites/${product.barCode}")
                            .removeValue()
                        bookmarkText = getString(R.string.bookmaked,product.name)
                    } else {
                        db.getReference("users/${user.uid}/favorites/${product.barCode}")
                            .setValue(product.toHistory(0))
                        bookmarkText = getString(R.string.unbookmarked, product.name)
                    }
                    Toast.makeText(context, bookmarkText, Toast.LENGTH_SHORT).show()
                }

                override fun onCancelled(p0: DatabaseError) {
                    throw p0.toException()
                }
            })
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        var barcode = data?. getStringExtra("SCAN_RESULT")
        val k = Intent(context, ProductActivity::class.java)
        k.putExtra("barcode", barcode)
        startActivity(k)
    }


}


interface OnProductClickListener {
    fun onProductClicked(product: Product)
    fun onBookmarkClicked(product: Product, view: View)
}
