package com.lea.chaminadas.yuka.api

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.lea.chaminadas.yuka.product.Product
import kotlinx.coroutines.Deferred
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query


public interface API{
    @GET("getProduct")
    fun getProductByBarCode(@Query("barcode") barcode: String): Deferred<ServerResponse>
}

class NetworkManager{
    val api: API = Retrofit.Builder()
            .baseUrl("https://api.formation-android.fr")
            .addConverterFactory(
                GsonConverterFactory.create()
            )
            .addCallAdapterFactory(
                CoroutineCallAdapterFactory()
            )
            .build()
            .create(API::class.java)

    suspend fun getProductByBarcode(barcode: String) : Product? {
        return try {
            val serverResponse: ServerResponse = api.getProductByBarCode(barcode).await()
            serverResponse.response?.toProduct()
        } catch (e: Exception) {
            null
        }
    }
}

