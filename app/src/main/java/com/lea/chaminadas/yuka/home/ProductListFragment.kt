package com.lea.chaminadas.yuka.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.lea.chaminadas.yuka.R
import com.lea.chaminadas.yuka.product.ProductWithFav
import kotlinx.android.synthetic.main.fragment_product_list.*

class ProductListFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_product_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        product_list.layoutManager = LinearLayoutManager(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val user = auth.currentUser
        if(user == null){

        }else {
            reference = db.getReference("/users/${user.uid}/")

            listener = reference!!.addValueEventListener(object :
                ValueEventListener {
                override fun onDataChange(p0: DataSnapshot) {
                    val products = mutableListOf<ProductWithFav>()

                    val barcodes = mutableListOf<String>()

                    val favsKeys = p0.child("favorites").children.map { it.key }
                    val history = p0.child("history").children

                    for (historyItem in history) {
                        if (historyItem != null) {

                            val item = historyItem.getValue(History::class.java)
                            if (item != null) {

                                val isFav = if (item.productBarcode in favsKeys) true else false
                                products.add(ProductWithFav(isFav, item.toProduct()))
                            }
                        }

                    }

                    displayList(products)
                }

                override fun onCancelled(p0: DatabaseError) {
                    throw p0.toException()
                }
            })
        }
    }

    override fun onPause() {
        super.onPause()

        if (listener != null) {
            reference?.removeEventListener(listener!!)
        }
    }

    companion object {
        fun newInstance(): ProductListFragment {
            val fragment = ProductListFragment()
            return fragment
        }
    }
}