package com.lea.chaminadas.yuka.api

import com.google.gson.annotations.SerializedName
import com.lea.chaminadas.yuka.product.NutritionFacts
import com.lea.chaminadas.yuka.product.NutritionFactsItem
import com.lea.chaminadas.yuka.product.Product


data class ServerResponse(
    @SerializedName("error")
    val error: String?,
    @SerializedName("response")
    val response: Response?
) {
    data class Response(
        @SerializedName("additives")
        val additives: Map<String, String>?,
        @SerializedName("allergens")
        val allergens: List<String?>?,
        @SerializedName("altName")
        val altName: String?,
        @SerializedName("barcode")
        val barcode: String,
        @SerializedName("brands")
        val brands: List<String?>?,
        @SerializedName("containsPalmOil")
        val containsPalmOil: Boolean?,
        @SerializedName("ingredients")
        val ingredients: List<String?>?,
        @SerializedName("manufacturingCountries")
        val manufacturingCountries: List<String?>?,
        @SerializedName("name")
        val name: String?,
        @SerializedName("novaScore")
        val novaScore: String?,
        @SerializedName("nutriScore")
        val nutriScore: String?,
        @SerializedName("nutritionFacts")
        val nutritionFacts: NutritionFacts?,
        @SerializedName("picture")
        val picture: String?,
        @SerializedName("quantity")
        val quantity: String?,
        @SerializedName("traces")
        val traces: List<String?>?
    ){
        fun toProduct() : Product {
            val realName = if(name.isNullOrEmpty()) "N/A" else name
            val realBrands = if(brands.isNullOrEmpty()) listOf("N/A") else brands
            val realBarcode = if(barcode.isNullOrEmpty()) "N/A" else barcode
            val realQuantity = if(quantity.isNullOrEmpty()) "N/A" else quantity
            val realCountrySold = if(manufacturingCountries.isNullOrEmpty()) listOf("N/A") else manufacturingCountries
            val realIngredients = if(ingredients.isNullOrEmpty()) listOf("N/A") else ingredients
            val realAllergies = if(allergens.isNullOrEmpty()) listOf("N/A") else allergens
            return Product(
                name = realName,
                brand = realBrands,
                barCode = realBarcode,
                url = picture,
                quantity = realQuantity,
                nutriScore = nutriScore,
                countrySold = realCountrySold,
                ingredients = realIngredients,
                allergies = realAllergies,
                nutritionFacts = nutritionFacts?.toNutritionFacts(),
                additives = additives
            )
        }
    }

    data class NutritionFacts(
        @SerializedName("calories")
        val calories: NutritionFact?,
        @SerializedName("carbohydrate")
        val carbohydrate: NutritionFact?,
        @SerializedName("energy")
        val energy: NutritionFact?,
        @SerializedName("fat")
        val fat: NutritionFact?,
        @SerializedName("fiber")
        val fiber: NutritionFact?,
        @SerializedName("proteins")
        val proteins: NutritionFact?,
        @SerializedName("salt")
        val salt: NutritionFact?,
        @SerializedName("saturatedFat")
        val saturatedFat: NutritionFact?,
        //@SerializedName("servingSize")
        //val servingSize: String?,
        @SerializedName("sodium")
        val sodium: NutritionFact?,
        @SerializedName("sugar")
        val sugar: NutritionFact?
    ) {
        fun toNutritionFacts() = NutritionFacts(
            energy = calories?.toNutritionFactItem(),
            carbohydrate = carbohydrate?.toNutritionFactItem(),
            fat = fat?.toNutritionFactItem(),
            fibers = fiber?.toNutritionFactItem(),
            salt = salt?.toNutritionFactItem(),
            sodium = sodium?.toNutritionFactItem(),
            sugar = sugar?.toNutritionFactItem(),
            proteins = proteins?.toNutritionFactItem(),
            saturatedFat = saturatedFat?.toNutritionFactItem()
        )
        data class NutritionFact(
            @SerializedName("per100g")
            val per100g: String?,
            @SerializedName("perServing")
            val perServing: String?,
            @SerializedName("unit")
            val unit: String?
        ){
            fun toNutritionFactItem() : NutritionFactsItem {
                val realUnit = if(unit.isNullOrEmpty()) "g" else unit
                val realQuantity = perServing
                val realQuantity100 = per100g
                return NutritionFactsItem(
                    quantity = realQuantity,
                    quantity100 = realQuantity100,
                    unit = realUnit
                )
            }
        }
    }
}
