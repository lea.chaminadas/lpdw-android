package com.lea.chaminadas.yuka.product

import android.graphics.PorterDuff
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.lea.chaminadas.yuka.home.OnProductClickListener
import com.lea.chaminadas.yuka.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.product_list_item.view.*

class ProductCell(v: View) : RecyclerView.ViewHolder(v) {
    val productImage: ImageView = v.product_image
    val productName: TextView = v.product_title
    val productBrand: TextView = v.product_brand
    val productNutriScore: TextView = v.product_nutri_score
    val productEnergy: TextView = v.product_energy
    val productBookmark: ImageView = v.product_bookmark
    val productCardView: View = v.product_card_view


    fun bindProduct(
        productWithFav: ProductWithFav,
        listener: OnProductClickListener
    ) {
        val product = productWithFav.product

        Picasso.get().load(product.url).into(productImage)
        productName.text = product.name
        productBrand.text = product.brand?.joinToString()
        productNutriScore.text = itemView.context.getString(R.string.nutriscore_label, product.nutriScore?.toUpperCase())
        var energyUnit = if (product.nutritionFacts?.energy?.unit.isNullOrEmpty()) "g" else product.nutritionFacts?.energy?.unit
        var energy = if (product.nutritionFacts?.energy?.quantity.isNullOrEmpty()) "N/A" else "${product.nutritionFacts?.energy?.quantity}${energyUnit}"



        productEnergy.text = itemView.context.getString(
            R.string.energy_label,
            energy
        )

        if (productWithFav.favorite) {
            productBookmark.setColorFilter(ContextCompat.getColor(itemView.context,
                R.color.colorPrimary
            ), PorterDuff.Mode.SRC_ATOP)
        } else {
            productBookmark.clearColorFilter()
        }

        productCardView.setOnClickListener {
            listener.onProductClicked(product)
        }

        productBookmark.setOnClickListener {
            listener.onBookmarkClicked(product, it)
        }

    }
}

class ProductWithFav(
    val favorite: Boolean,
    val product: Product
)