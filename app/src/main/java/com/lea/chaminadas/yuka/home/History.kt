package com.lea.chaminadas.yuka.home

import com.lea.chaminadas.yuka.product.NutritionFacts
import com.lea.chaminadas.yuka.product.NutritionFactsItem
import com.lea.chaminadas.yuka.product.Product

data class History(
    val id: Long,
    val productBarcode: String,
    val productBrands: List<String?>,
    val productImageUrl: String?,
    val productName: String,
    val productNutriScore: String,
    val productEnergy: String?
){
    constructor() : this(0, "", listOf(), null, "", "", "")

    fun toProduct() = Product(
        name = productName,
        brand = productBrands,
        url = productImageUrl,
        nutriScore = productNutriScore,
        barCode = productBarcode,
        nutritionFacts = NutritionFacts(
            energy = NutritionFactsItem(null, productEnergy, null),
            salt = null,
            saturatedFat = null,
            proteins = null,
            sugar = null,
            fibers = null,
            carbohydrate = null,
            fat = null,
            sodium = null

        ),
        allergies = listOf(),
        quantity = "",
        additives = mapOf("" to ""),
        ingredients = listOf(""),
        countrySold = listOf("")
    )
}