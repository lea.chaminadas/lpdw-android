package com.lea.chaminadas.yuka

class User {
    var name: String = ""

    // Obligatoire
    constructor() {}

    constructor(name: String) {
        this.name = name
    }
}