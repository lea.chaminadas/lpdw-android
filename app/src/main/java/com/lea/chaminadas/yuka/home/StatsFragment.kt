package com.lea.chaminadas.yuka.home


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.lea.chaminadas.yuka.R


class StatsFragment : Fragment() {

    lateinit var stats: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if(arguments == null || arguments?.containsKey("stats") == false){
            throw Exception("Stats is missing")
        }
        stats = arguments!!.getString("stats")

        return inflater.inflate(R.layout.fragment_stats, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    companion object {
        fun newInstance(stats: String) : StatsFragment {
            val fragment = StatsFragment()
            val args = Bundle()
            args.putString("stats", stats)
            fragment.arguments = args
            return fragment
        }
    }


}
