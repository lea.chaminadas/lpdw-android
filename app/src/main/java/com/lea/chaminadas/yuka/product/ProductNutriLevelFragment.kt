package com.lea.chaminadas.yuka.product

import android.content.res.ColorStateList
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.fragment.app.Fragment
import com.lea.chaminadas.yuka.R
import kotlinx.android.synthetic.main.fragment_product_nutri_level.*

class ProductNutriLevelFragment : Fragment(){
    lateinit var product: Product

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_product_nutri_level, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        if(arguments == null || arguments?.containsKey("product") == false){
            throw Exception("Product is missing")
        }

        product = arguments!!.getParcelable("product")


        val fat_level_string: String = getLevelString(product.nutritionFacts?.fat?.quantity100?.toDouble(), 3.0, 20.0, product_fat_level_icon)
        val fat_saturated_level_string: String = getLevelString(product.nutritionFacts?.saturatedFat?.quantity100?.toDouble(), 1.5, 5.0, product_fat_saturated_level_icon)
        val sugar_level_string: String = getLevelString(product.nutritionFacts?.sugar?.quantity100?.toDouble(), 5.0, 12.5, product_sugar_level_icon)
        val salt_level_string: String = getLevelString(product.nutritionFacts?.salt?.quantity100?.toDouble(), 0.3, 1.5, product_salt_level_icon)

        product_fat_level_label.text = if(!fat_level_string.isNullOrEmpty()){
            Html.fromHtml(getString(R.string.fat_level, "${product.nutritionFacts?.fat?.quantity100}${product.nutritionFacts?.fat?.unit}", fat_level_string))
        } else {
            "${getString(R.string.fat)} : ${getString(R.string.no_value)}"
        }
        product_fat_saturated_level_label.text = if(!fat_saturated_level_string.isNullOrEmpty()){
            Html.fromHtml(getString(R.string.fat_saturated_level, "${product.nutritionFacts?.saturatedFat?.quantity100}${product.nutritionFacts?.saturatedFat?.unit}", fat_saturated_level_string))
        } else {
            "${getString(R.string.fat_saturated)} : ${getString(R.string.no_value)}"
        }
        product_sugar_level_label.text = if(!sugar_level_string.isNullOrEmpty()){
            Html.fromHtml(getString(R.string.sugar_level, "${product.nutritionFacts?.sugar?.quantity100}${product.nutritionFacts?.sugar?.unit}", sugar_level_string))
        } else {
            "${getString(R.string.sugar)} : ${getString(R.string.no_value)}"
        }
        product_salt_level_label.text = if(!fat_level_string.isNullOrEmpty()){
            Html.fromHtml(getString(R.string.salt_level, "${product.nutritionFacts?.salt?.quantity100}${product.nutritionFacts?.salt?.unit}", salt_level_string))
        } else {
            "${getString(R.string.salt)} : ${getString(R.string.no_value)}"
        }

    }

    fun getLevelString(quantity: Double?, bottomFork: Double, topFork: Double, ovalView: View): String{
        val high_level_string: String = getString(R.string.high_quantity)
        val moderate_level_string: String = getString(R.string.moderate_quantity)
        val low_level_string: String = getString(R.string.low_quantity)

        if (quantity != null) {
            if(!quantity.isNaN() ){
                if(quantity < bottomFork){
                    DrawableCompat.setTintList(ovalView.background, ColorStateList.valueOf(ContextCompat.getColor(requireContext(),
                        R.color.nutrient_level_low
                    )))
                    return low_level_string
                }else if (quantity <= topFork){
                    DrawableCompat.setTintList(ovalView.background, ColorStateList.valueOf(ContextCompat.getColor(requireContext(),
                        R.color.nutrient_level_moderate
                    )))
                    return moderate_level_string
                }else{
                    DrawableCompat.setTintList(ovalView.background, ColorStateList.valueOf(ContextCompat.getColor(requireContext(),
                        R.color.nutrient_level_high
                    )))
                    return high_level_string
                }
            }
        }

        return ""
    }

    companion object {
        fun newInstance(product: Product) : ProductNutriLevelFragment {
            val fragment = ProductNutriLevelFragment()
            val args = Bundle()
            args.putParcelable("product", product)
            fragment.arguments = args
            return fragment
        }
    }
}