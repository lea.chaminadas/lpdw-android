package com.lea.chaminadas.yuka.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lea.chaminadas.yuka.R
import com.lea.chaminadas.yuka.product.ProductCell
import com.lea.chaminadas.yuka.product.ProductWithFav

class ProductListAdapter(val products: List<ProductWithFav>, val listener: OnProductClickListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    override fun getItemCount(): Int {
        return products.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ProductCell).bindProduct(products[position], listener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ProductCell(inflater.inflate(R.layout.product_list_item, parent, false))
    }

}