package com.lea.chaminadas.yuka.product

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.lea.chaminadas.yuka.R
import kotlinx.android.synthetic.main.fragment_product_nutri_info.*

class ProductNutriInfoFragment : Fragment() {
    lateinit var product: Product

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(arguments == null || arguments?.containsKey("product") == false){
            throw Exception("Product is missing")
        }
        product = arguments!!.getParcelable("product")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_product_nutri_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var nutritionFacts= product.nutritionFacts

        if(nutritionFacts != null){

            val energy = nutritionFacts.energy
            val fat = nutritionFacts.fat
            val carbohydrate = nutritionFacts.carbohydrate
            val sodium = nutritionFacts.sodium
            val salt = nutritionFacts.salt
            val proteins = nutritionFacts.proteins
            val fibers = nutritionFacts.fibers
            val saturatedFat = nutritionFacts.saturatedFat
            val sugar = nutritionFacts.sugar

            if(energy != null){
                if(energy.quantity100 != "") energy_for_100g.text = energy.quantity100 + energy.unit
                if(energy.quantity!= "") energy_for_portion.text = energy.quantity + energy.unit
            }

            if(fat != null) {
                if (fat.quantity100 != "") fat_for_100g.text = fat.quantity100 + fat.unit
                if (fat.quantity != "") fat_for_portion.text = fat.quantity + fat.unit
            }

            if(carbohydrate != null) {
                if (carbohydrate.quantity100 != "") carbohydrate_for_100g.text =
                     carbohydrate.quantity100 + carbohydrate.unit
                if (carbohydrate.quantity != "") carbohydrate_for_portion.text = carbohydrate.quantity + carbohydrate.unit
            }

            if(sodium != null) {
                var quantity100: String = ""
                if(sodium.quantity100.isNullOrEmpty()){
                    quantity100 = "%.4f".format(sodium.quantity100?.toDouble())
                    sodium_for_100g.text = quantity100 + sodium.unit
                }
                if (sodium.quantity != "") sodium_for_portion.text = sodium.quantity + sodium.unit
            }

            if(salt != null) {
                if (!salt.quantity100.isNullOrEmpty()) salt_for_100g.text = (salt.quantity100 + salt.unit)
                if (!salt.quantity.isNullOrEmpty())  salt_for_portion.text = (salt.quantity + salt.unit)
            }

            if(proteins != null) {
                if (proteins.quantity100 != "") proteins_for_100g.text = proteins.quantity100 + proteins.unit
                if (proteins.quantity != "") proteins_for_portion.text = proteins.quantity + proteins.unit
            }

            if(fibers != null) {
                if (fibers.quantity100 != "") fiber_for_100g.text =  fibers.quantity100 + fibers.unit
                if (fibers.quantity != "") fiber_for_portion.text = fibers.quantity + fibers.unit
            }

            if(saturatedFat != null) {
                if (saturatedFat.quantity100 != "")  fat_saturated_for_100g.text =
                    saturatedFat.quantity100 + saturatedFat.unit
                if (saturatedFat.quantity != "") fat_saturated_for_portion.text = saturatedFat.quantity + saturatedFat.unit
            }

            if(sugar != null) {
                if (sugar.quantity100 != "")  sugar_for_100g.text =
                    sugar.quantity100 + sugar.unit
                if (sugar.quantity != "") sugar_for_portion.text = sugar.quantity + sugar.unit
            }
        }



    }

    companion object {
        fun newInstance(product: Product) : ProductNutriInfoFragment {
            val fragment = ProductNutriInfoFragment()
            val args = Bundle()
            args.putParcelable("product", product)
            fragment.arguments = args
            return fragment
        }
    }
}