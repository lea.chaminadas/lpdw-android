package com.lea.chaminadas.yuka.product

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.lea.chaminadas.yuka.AuthActivity
import com.lea.chaminadas.yuka.api.NetworkManager
import com.lea.chaminadas.yuka.R
import kotlinx.android.synthetic.main.activity_product.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ProductActivity : AppCompatActivity() {
    val database = FirebaseDatabase.getInstance()
    val auth: FirebaseAuth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val user = auth.currentUser

        if(user !=null) {

            setContentView(R.layout.activity_product)

            setSupportActionBar(toolbar)

            supportActionBar?.setBackgroundDrawable(ContextCompat.getDrawable(this,
                R.drawable.bg_toolbar
            ))
            supportActionBar?.setTitle(R.string.product_activity_title)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)

            if (!intent.hasExtra("barcode")) {
                throw Exception("Product missing")
            }

            val barcode = intent!!.getStringExtra("barcode");

            GlobalScope.launch(Dispatchers.Main) {
                progressBar.visibility = View.VISIBLE
                try {
                    val product = withContext(Dispatchers.IO)
                    { NetworkManager().getProductByBarcode(barcode) }

                    if (product != null && product.barCode.isNotEmpty() && product.barCode != "N/A") {

                        pager.adapter = ProductFragmentAdapter(
                            supportFragmentManager,
                            product,
                            applicationContext
                        )
                        tabs.setupWithViewPager(pager)
                        tabs.setSelectedTabIndicatorColor(Color.WHITE)

                        val today = System.currentTimeMillis()
                        database.getReference("users/${user.uid}/history/${today}")
                            .setValue(product.toHistory(today))
                        progressBar.visibility = View.GONE
                    } else {
                        progressBar.visibility = View.GONE
                        Toast.makeText(pager.context, "Product not found", Toast.LENGTH_LONG).show()
                        finish()
                    }
                } catch (e: Exception) {
                    throw e
                    Toast.makeText(pager.context, "Servor error", Toast.LENGTH_LONG).show()
                    progressBar.visibility = View.GONE
                }
            }
        } else {
            val k = Intent(applicationContext, AuthActivity::class.java)
            startActivity(k)
        }

    }

//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        when (item.getItemId()) {
//            android.R.id.home -> {
//                finish()
//                return true
//            }
//        }
//
//        return super.onOptionsItemSelected(item)
//    }
//
//    override fun onCreateOptionsMenu(menu: Menu): Boolean {
//        return true
//    }
}
